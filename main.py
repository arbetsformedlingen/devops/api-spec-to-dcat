import os
import sys
import argparse
import logging
import uuid
from modules import file_operations
from modules.dcat_processor import (
    create_artifact,
    create_flat_dir,
    process_directory_dcat,
    process_directory_link_dcat,
    validate_and_move_dcat_files,
)

# Configure logging
logging.basicConfig(level=logging.INFO, format="%(levelname)s: %(message)s")


def main():
    parser = argparse.ArgumentParser(description="DCAT Processing Tool", add_help=False)
    parser.add_argument(
        "-h", "--help", action="store_true", help="Print arguments and exit"
    )
    parser.add_argument("--env", default="test", help="Environment (default: test)")
    args = parser.parse_args()
    if args.help:
        help_text = """
DCAT Processing Tool

Usage:
    python main.py [OPTIONS]

Options:
    -h, --help           Show this help message and exit.
    --env ENVIRONMENT    Specify the environment. Default is 'test'.

Environment Variables:
    BUCKET               S3 bucket used for file uploads (default: data.jobtechdev.se-adhoc-test)
    BASEURL              Base URL for downloading files (default: https://data-develop.jobtechdev.se/)
    DCATJAR              Path to the DCAT processor JAR (default: /app/app.jar)
    API_URL_LIST         URL to the API specification list (optional)

Process Overview:
    1. Download API specifications.
    2. Download S3 metadata and convert to Swagger format.
    3. Process DCAT JSON files.
    4. Validate and move DCAT files.
    5. Create a flat directory for the DCAT processor.
    6. Compile the final DCAT artifact.
    7. Upload the artifact to S3.

For more details, please refer to the project documentation.
"""
        print(help_text)
        sys.exit(0)

    required_tools = ["java", "aws"]
    file_operations.verify_env(required_tools)

    file_operations.create_directories(["target", "staging"])

    # Define variables similar to Makefile
    BUCKET = os.getenv("BUCKET", "data.jobtechdev.se-adhoc-test")
    BASEURL = os.getenv("BASEURL", "https://data-develop.jobtechdev.se/")
    DCATJAR = os.getenv("DCATJAR", "/app/app.jar")

    # URL list based on environment
    if args.env == "test":
        api_url_list = "https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-prepare-static-web/-/raw/main/generate_dcat/conf/api-json-spec-urls-test.txt"
    else:
        api_url_list = "https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-prepare-static-web/-/raw/main/generate_dcat/conf/api-json-spec-urls.txt"
    api_url_list = os.getenv("API_URL_LIST", api_url_list)

    logging.info(f"Using api-url-list: {api_url_list}")

    # Step 1: Download API specifications
    api_urls = file_operations.download_file_list(api_url_list)
    if not api_urls:
        logging.error("No API URLs found. Exiting.")
        sys.exit(1)

    for url in api_urls:
        destination = os.path.join("staging", f"{uuid.uuid4()}.dcat.json")
        logging.info(
            f"Downloading url from api-list {url} into {destination} - this file is already in swagger format and does not need to be converted/processed."
        )
        file_operations.download_file(url, destination)

    # Step 2: Download S3 specifications
    file_operations.download_and_convert_metadata(BUCKET, "staging", BASEURL)

    # Step 3: Process directory.dcat.json and directory_link.dcat.json
    process_directory_link_dcat(BUCKET, BASEURL)
    process_directory_dcat(BUCKET, BASEURL)

    # Step 4: Validate specifications
    validate_and_move_dcat_files("staging", "target", DCATJAR, "conf/catalog.json")

    # dcat-processor requires a flat directory structure
    abs_flat_dir = create_flat_dir(
        "target", os.path.abspath(os.path.join("target", "flat"))
    )

    # Step 5: Compile DCAT
    artifact = os.path.join(abs_flat_dir, "jobtech.dcat.xml")
    create_artifact(abs_flat_dir, artifact, "conf/catalog.json", DCATJAR)

    # Step 6: Upload artifact to S3
    file_operations.upload_artifact(BUCKET, artifact, "dcatotron/jobtech.dcat.xml")


if __name__ == "__main__":
    main()
