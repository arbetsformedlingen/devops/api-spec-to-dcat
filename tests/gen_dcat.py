import shutil
import subprocess
import sys
import os
import tempfile
import requests


def download_file(url, destination):
    response = requests.get(url)
    response.raise_for_status()
    with open(destination, "wb") as f:
        f.write(response.content)


def main():
    with tempfile.TemporaryDirectory() as jardir:
        app_url = "https://gitlab.com/arbetsformedlingen/devops/dcat-ap-se-processor-mirror/-/jobs/8878887365/artifacts/raw/target/dcat-ap-processor-0.0.2-SNAPSHOT.jar"
        app_path = os.path.join(jardir, "app.jar")
        try:
            download_file(app_url, app_path)
        except requests.HTTPError as e:
            print(f"ERROR: Fel vid nedladdning av app.jar: {e}", file=sys.stderr)
            sys.exit(1)

        required_tools = ["java", "grep"]
        for tool in required_tools:
            if not shutil.which(tool):
                print(f"ERROR: saknar verktyg {tool}", file=sys.stderr)
                sys.exit(1)

        with tempfile.TemporaryDirectory() as tmpd:
            os.chdir(tmpd)
            metadata_url = "https://github.com/diggsweden/DCAT-AP-SE-Processor/raw/18-f%C3%B6rvaltning-av-gamla-beroenden-1/src/main/resources/metadataExample/single/full_example.json"
            try:
                download_file(metadata_url, "full_example.json")
            except requests.HTTPError as e:
                print(f"ERROR: Fel vid nedladdning av metadata: {e}", file=sys.stderr)
                sys.exit(1)

            try:
                with open("dcat.rdf", "w", encoding="utf-8") as f_out:
                    subprocess.run(
                        ["java", "-jar", app_path, "-f", "full_example.json"],
                        check=True,
                        stdout=f_out,
                        cwd=tmpd,
                    )
            except subprocess.CalledProcessError:
                print("ERROR: Test misslyckades vid DCAT-generering.", file=sys.stderr)
                sys.exit(1)

            with open("dcat.rdf", "r", encoding="utf-8") as f:
                content = f.read()
                if "Kunde inte generera en dcat fil" in content:
                    print(
                        "ERROR: Test misslyckades vid DCAT-generering.", file=sys.stderr
                    )
                    sys.exit(1)

            # Rensa temporära filer
            os.remove("full_example.json")
            os.remove("dcat.rdf")


if __name__ == "__main__":
    main()
