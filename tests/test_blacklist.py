import unittest
from modules.blacklist import is_blacklisted


class TestBlacklist(unittest.TestCase):
    def test_blacklisted_strings(self):
        blacklisted = [
            "index.html",
            "public-tokens",
            "dcatotron",
            "Nginx-Fancyindex-Theme-light",
            "swagger",
            "rss",
            "50x.html",
            "README",
            "directory.dcat.json",
            "collection.dcat.json",
        ]
        for item in blacklisted:
            with self.subTest(item=item):
                self.assertTrue(is_blacklisted(item))

    def test_non_blacklisted_strings(self):
        non_blacklisted = [
            "home.html",
            "token-public",
            "swagger_v2",
            "README.md",
            "data.json",
            "config.yaml",
        ]
        for item in non_blacklisted:
            with self.subTest(item=item):
                self.assertFalse(is_blacklisted(item))

    def test_empty_string(self):
        self.assertFalse(is_blacklisted(""))

    def test_special_characters(self):
        self.assertFalse(is_blacklisted("@#!$%^&*()"))
