import unittest
from unittest.mock import patch, mock_open, MagicMock
from modules.dcat_processor import (
    validate_dcat,
    create_flat_dir,
    create_artifact,
    validate_and_move_dcat_files,
)
import subprocess
import os


class TestDcatProcessor(unittest.TestCase):
    @patch("modules.dcat_processor.tempfile.mkdtemp", return_value="tmpdir")
    @patch("modules.dcat_processor.subprocess.run")
    @patch("builtins.open", new_callable=mock_open)
    @patch("modules.dcat_processor.os.path.exists")
    @patch("modules.dcat_processor.shutil.copy")
    @patch("modules.dcat_processor.os.remove")
    def test_validate_dcat_success(
        self,
        mock_remove,
        mock_copy,
        mock_exists,
        mock_file,
        mock_subprocess_run,
        mock_mkdtemp
    ):
        mock_exists.return_value = True
        mock_subprocess_run.return_value = MagicMock(
            stdout="Validation successful", stderr=""
        )

        # Use absolute path for testing but expect file copied into tmpdir
        input_file = os.path.abspath("input_file.json")
        result = validate_dcat(input_file, "dcat.jar", "target_file.xml", "catalog.json")
        self.assertTrue(result)

        # Verify file operations: expect two copy calls with tmpdir
        self.assertEqual(mock_copy.call_count, 2)
        expected_calls = [
            unittest.mock.call("catalog.json", os.path.join("tmpdir", "catalog.json")),
            unittest.mock.call(input_file, "tmpdir"),
        ]
        mock_copy.assert_has_calls(expected_calls, any_order=True)

        # Verify subprocess call with file in tmpdir
        mock_subprocess_run.assert_called_once_with(
            ["java", "-jar", "dcat.jar", "-d", "tmpdir"],
            capture_output=True,
            text=True,
            check=True,
            cwd="tmpdir",
            env=unittest.mock.ANY,
        )

    @patch("modules.dcat_processor.tempfile.mkdtemp", return_value="tmpdir")
    @patch(
        "modules.dcat_processor.subprocess.run",
        side_effect=subprocess.CalledProcessError(1, "cmd"),
    )
    @patch("modules.dcat_processor.os.path.exists", return_value=True)
    @patch("modules.dcat_processor.shutil.copy")
    @patch("modules.dcat_processor.os.remove")
    def test_validate_dcat_subprocess_error(
        self,
        mock_remove,
        mock_copy,
        mock_exists,
        mock_subprocess_run,
        mock_mkdtemp
    ):
        result = validate_dcat("input_file.json", "dcat.jar", "target_file.xml", "catalog.json")
        self.assertFalse(result)
        
        # Verify cleanup still happens on error
        self.assertEqual(mock_copy.call_count, 2)
        expected_calls = [
            unittest.mock.call("catalog.json", os.path.join("tmpdir", "catalog.json")),
            unittest.mock.call("input_file.json", "tmpdir"),
        ]
        mock_copy.assert_has_calls(expected_calls, any_order=True)

    @patch("modules.dcat_processor.os.makedirs")
    @patch("modules.dcat_processor.shutil.copy2")
    @patch("modules.dcat_processor.os.walk")
    def test_create_flat_dir(self, mock_walk, mock_copy2, mock_makedirs):
        mock_walk.return_value = [
            ("meta_dir", ("subdir",), ("file1.json", "file2.json")),
            ("meta_dir/subdir", (), ("file3.json",)),
        ]
        result = create_flat_dir("meta_dir", "abs_flat_dir")
        self.assertEqual(result, "abs_flat_dir")
        mock_makedirs.assert_called_once_with("abs_flat_dir", exist_ok=True)
        self.assertEqual(mock_copy2.call_count, 3)

    @patch("modules.dcat_processor.subprocess.run")
    @patch("builtins.open", new_callable=mock_open)
    @patch("modules.dcat_processor.shutil.copy")
    def test_create_artifact_success(self, mock_copy, mock_file, mock_subprocess_run):
        mock_subprocess_run.return_value = MagicMock(
            stdout="Artifact creation successful", stderr=""
        )
        with patch(
            "modules.dcat_processor.os.path.join", side_effect=lambda a, b: f"{a}/{b}"
        ):
            create_artifact(
                "/abs_flat_dir",
                "target/artifact.dcat.xml",
                "conf/catalog.json",
                "dcat.jar",
            )

        mock_copy.assert_called_once_with(
            "conf/catalog.json", "/abs_flat_dir/catalog.json"
        )
        mock_subprocess_run.assert_called_once_with(
            ["java", "-jar", "dcat.jar", "-d", "/abs_flat_dir"],
            capture_output=True,
            text=True,
            check=True,
            cwd="/abs_flat_dir",
            env=unittest.mock.ANY,
        )
        mock_file.assert_called_once_with(
            "target/artifact.dcat.xml", "w", encoding="utf-8"
        )

    @patch("modules.dcat_processor.shutil.move")
    @patch("modules.dcat_processor.validate_dcat")
    @patch("modules.dcat_processor.os.remove")
    @patch("modules.dcat_processor.os.walk")
    def test_validate_and_move_dcat_files(
        self, mock_walk, mock_remove, mock_validate_dcat, mock_move
    ):
        mock_walk.return_value = [
            ("staging", (), ("file1.dcat.json", "file2.dcat.json")),
        ]
        mock_validate_dcat.side_effect = [True, False]

        validate_and_move_dcat_files("staging", "target", "dcat.jar", "catalog.json")

        mock_validate_dcat.assert_any_call(
            "staging/file1.dcat.json", "dcat.jar", "staging/file1.dcat.xml", "catalog.json"
        )
        mock_validate_dcat.assert_any_call(
            "staging/file2.dcat.json", "dcat.jar", "staging/file2.dcat.xml", "catalog.json"
        )
        mock_move.assert_called_once_with(
            "staging/file1.dcat.json", "target/file1.dcat.json"
        )
        mock_remove.assert_called_once_with("staging/file2.dcat.json")

    # More tests can be added for process_directory_dcat and process_directory_link_dcat in a similar way
