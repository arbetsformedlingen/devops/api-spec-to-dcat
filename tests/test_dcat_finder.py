import unittest
from unittest.mock import patch, MagicMock
from modules.dcat_finder import find_dcat_files
import subprocess


class TestDcatFinder(unittest.TestCase):
    @patch("modules.dcat_finder.subprocess.run")
    @patch("modules.dcat_finder.is_blacklisted")
    def test_find_dcat_files_success(self, mock_is_blacklisted, mock_subprocess_run):
        # Simulate S3 ls output where the filename includes spaces.
        mock_subprocess_run.return_value = MagicMock(
            stdout="""
2025-01-27 13:08:46     447309 yrkesprognos.json
2025-01-27 13:08:46       3809 yrkesprognos file with space.dcat.json
2025-02-20 12:13:13       8600 yrkesprognos.json.dcat.xml
"""
        )
        # Do not blacklist any file.
        mock_is_blacklisted.side_effect = lambda x: False

        result = find_dcat_files("test-bucket")
        self.assertEqual(result, ["yrkesprognos file with space.dcat.json"])
        mock_subprocess_run.assert_called_once_with(
            ["aws", "s3", "ls", "--recursive", "s3://test-bucket/"],
            stdout=subprocess.PIPE,
            check=True,
            text=True,
        )

    @patch("modules.dcat_finder.subprocess.run")
    def test_find_dcat_files_no_matches(self, mock_subprocess_run):
        mock_subprocess_run.return_value = MagicMock(
            stdout="""
2025-01-27 13:08:46     447309 yrkesprognos.json
2025-01-27 13:08:46       3809 yrkesprognos file with space.dcat.xml
2025-02-20 12:13:13       8600 yrkesprognos.json.dcat.xml
"""
        )
        result = find_dcat_files("empty-bucket")
        self.assertEqual(result, [])

    @patch(
        "modules.dcat_finder.subprocess.run",
        side_effect=subprocess.CalledProcessError(1, "cmd"),
    )
    def test_find_dcat_files_subprocess_error(self, mock_subprocess_run):
        result = find_dcat_files("error-bucket")
        self.assertEqual(result, [])
