import unittest
from unittest.mock import patch, mock_open
from modules import dcatjson2dcatswagger


class TestDcatJsonToDcatSwagger(unittest.TestCase):
    @patch("builtins.open", new_callable=mock_open)
    def test_convert_dcat_json_to_swagger(self, mock_file):
        # Mock contents of swaggerwrapper.json, catalog.json, and input dcat.json
        swaggerwrapper = """{
            "info": {
                "x-dcat": {
                    "dcat-dataset": {},
                    "dcat-catalog": {}
                }
            },
            "paths": {}
        }"""
        catalog = """{
            "dcat-catalog": {
                "name": "Test Catalog"
            }
        }"""
        input_dcat = """{
            "key": "value"
        }"""

        # Create mock file handlers for reading and writing
        mock_wrapper = mock_open(read_data=swaggerwrapper).return_value
        mock_catalog = mock_open(read_data=catalog).return_value
        mock_input = mock_open(read_data=input_dcat).return_value
        mock_output = mock_open().return_value

        # Configure mock to return different content based on the file path
        def mock_file_handler(*args, **kwargs):
            if args[0] == "conf/swaggerwrapper.json":
                return mock_wrapper
            elif args[0] == "conf/catalog.json":
                return mock_catalog
            elif args[0] == "input.dcat.json":
                return mock_input
            return mock_output

        mock_file.side_effect = mock_file_handler

        # Execute the function
        with patch(
            "modules.dcatjson2dcatswagger.os.getenv", side_effect=lambda x, y=None: y
        ):
            dcatjson2dcatswagger.convert_dcat_json_to_swagger(
                input_file_path="input.dcat.json",
                input_id="http://example.com/input",
                target="output.json",
                access_url="http://example.com/access",
            )

        # Verify JSON content was written
        mock_output.write.assert_called()
