import unittest
import os
from unittest.mock import patch, MagicMock
from main import main


class TestMain(unittest.TestCase):
    @patch("main.file_operations.verify_env")
    @patch("main.file_operations.create_directories")
    @patch("main.file_operations.download_file_list")
    @patch("main.file_operations.download_file")
    @patch("main.file_operations.download_and_convert_metadata")
    @patch("main.process_directory_link_dcat")
    @patch("main.process_directory_dcat")
    @patch("main.validate_and_move_dcat_files")
    @patch("main.create_flat_dir")
    @patch("main.create_artifact")
    @patch("main.file_operations.upload_artifact")
    @patch("main.argparse.ArgumentParser.parse_args")
    @patch("main.sys.exit")
    def test_main_success(
        self,
        mock_exit,
        mock_parse_args,
        mock_upload_artifact,
        mock_create_artifact,
        mock_create_flat_dir,
        mock_validate_and_move_dcat_files,
        mock_process_directory_dcat,
        mock_process_directory_link_dcat,
        mock_download_and_convert_metadata,
        mock_download_file,
        mock_download_file_list,
        mock_create_directories,
        mock_verify_env,
    ):
        # Setup
        mock_parse_args.return_value = MagicMock(help=False, env="test")
        mock_download_file_list.return_value = ["http://example.com/api1.dcat.json"]
        mock_download_file.return_value = True
        mock_create_flat_dir.return_value = "/absolute/path/target/flat"

        # Run
        main()

        # Assert
        mock_verify_env.assert_called_once_with(["java", "aws"])
        mock_create_directories.assert_called_once_with(["target", "staging"])
        mock_download_file_list.assert_called_once_with(
            "https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-prepare-static-web/-/raw/main/generate_dcat/conf/api-json-spec-urls-test.txt"
        )
        mock_download_file.assert_called_once()  # File path will be dynamic due to UUID
        mock_download_and_convert_metadata.assert_called_once_with(
            "data.jobtechdev.se-adhoc-test",
            "staging",
            "https://data-develop.jobtechdev.se/",
        )
        mock_process_directory_link_dcat.assert_called_once_with(
            "data.jobtechdev.se-adhoc-test", "https://data-develop.jobtechdev.se/"
        )
        mock_process_directory_dcat.assert_called_once_with(
            "data.jobtechdev.se-adhoc-test", "https://data-develop.jobtechdev.se/"
        )
        mock_validate_and_move_dcat_files.assert_called_once_with(
            "staging", "target", "/app/app.jar", "conf/catalog.json"
        )
        mock_create_flat_dir.assert_called_once_with("target", unittest.mock.ANY)

        artifact_path = os.path.join(
            mock_create_flat_dir.return_value, "jobtech.dcat.xml"
        )
        mock_create_artifact.assert_called_once_with(
            mock_create_flat_dir.return_value,
            artifact_path,
            "conf/catalog.json",
            "/app/app.jar",
        )
        mock_upload_artifact.assert_called_once_with(
            "data.jobtechdev.se-adhoc-test", artifact_path, "dcatotron/jobtech.dcat.xml"
        )
        mock_exit.assert_not_called()
