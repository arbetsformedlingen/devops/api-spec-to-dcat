# Turning our dcat.json files into the upload artifact


## 1. Retrieving API specs 
```mermaid
stateDiagram-v2
    [*] --> Initialize
        
    Initialize --> DownloadAPIs
    
    state DownloadAPIs {
        [*] --> LoadURLList
        LoadURLList --> FetchAPISpecs
        FetchAPISpecs --> [*]
        
        note left of LoadURLList: Uses conf/api-json-spec-urls(-test).txt
    }
```

## 2. Download *.dcat.json from the S3 bucket
```mermaid
stateDiagram-v2
    state DownloadS3Metadata {
        [*] --> FindDCATFiles
        FindDCATFiles --> DownloadFiles
        DownloadFiles --> ConvertToSwagger
        ConvertToSwagger --> [*]
        
        note left of ConvertToSwagger: Uses conf/swaggerwrapper.json
    }
    
```

## 3. Handle special dcat.json files that describe a while directory of files
```mermaid
stateDiagram-v2
    
        [*] --> ProcessDirLink
        ProcessDirLink --> ConvertToSwagger
        ConvertToSwagger --> [*]
        
        note left of ConvertToSwagger: Uses conf/swaggerwrapper.json
        
        note left of ProcessDirLink: Uses conf/directory_link.dcat.json
 ```

## 4. Validate all files 
```mermaid
stateDiagram-v2
    
    state ValidateFiles {
        [*] --> ValidateDCAT
        ValidateDCAT --> MoveToTarget
        MoveToTarget --> [*]
        
        note right of ValidateDCAT: Uses conf/catalog.json
    }
```

## 5. Prepare a directory that DCAT-PROCESSOR can accept as input
```mermaid
stateDiagram-v2
    
    state CreateFlatDirectory {
        [*] --> CopyJSONFiles
        CopyJSONFiles --> AssignUniqueNames
        AssignUniqueNames --> [*]
    }
    
```

## 6. Run DCAT-PROCESSOR to produce uploadable artifact (the RDF-file)
```mermaid
stateDiagram-v2
    
        [*] --> RunDCATProcessor
        RunDCATProcessor --> [*]
        
        note right of RunDCATProcessor: Uses conf/catalog.json
```

## 7. Upload to dataporta.se
```mermaid
stateDiagram-v2
    
    [*] --> UploadToS3
            
    note right of UploadToS3: Uploads to dataportal.se, dcatotron/jobtech.dcat.xml
```

