# Automatic Conversion of API Specs to DCAT

This Python-based tool automates the conversion of API specifications to DCAT format.

## Configuration

- `conf/catalog.json` - The catalog description, general for all APIs.
- `conf/api-json-spec-urls.txt` - The list of API URLs.
- `conf/api-json-spec-urls-test.txt` - The list of API URLs for testing.

## Setup

### Local container

1. **Build Docker image:**

    ```bash
    docker build -t djs3 .
    ```

2. **Run container:**
   
   Create a directory containing AWS credentials:

    ```bash
    mkdir -p /tmp/tst
    # Add AWS config files to /tmp/tst:
    # - config
    # - credentials
    ```

    Then run the container:

    ```bash
    docker run -v /tmp/tst:/secrets --rm -it djs3
    ```

### OpenShift

This can be run as a cronjob in OpenShift. See configuration files here:
<https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-prepare-static-web-infra>

### Local Development

The project uses `uv` for Python dependency management. Make sure you have Python 3 installed.

1. **Install uv:**

    ```bash
    pip install uv
    ```

2. **Setup virtual environment and install dependencies:**

    ```bash
    uv venv
    uv pip sync
    ```

3. **Run the program:**

    ```bash
    uv run main.py --env test
    ```

### Run tests

Run test scripts to verify functionality.

```bash
python3 tests/gen_dcat.py
```

## Known limitations

- Does not handle RAML, YAML or Swagger/OpenAPI < 3.


# Automatic conversion of API specs to DCAT
This is a script that:
 1. Downloads all API specs which are listed in `conf/api-json-spec-urls.txt`
    into corresponding files with suffix `.dcat.json`.
 2. Downloads all files called `X.dcat.json` from S3 which have a
    corresponding file `X` (see `conf/example_dcat_for_datafile.json` for an example of how it could look).
 3. Downloads all files called `directory.dcat.json` and applies
    this base information, along with `conf/catalog.json` to all the data files `X,...,Y` in the same directory,
    creating `X.dcat.json, ..., Y.dcat.json`.
 4. Downloads all files called `collection.dcat.json` and creates a
    data object based on this information, along with
    `conf/catalog.json`. It also does a directory listing in the
    directory in which it was found, and includes it in the object
    description.
 5. Each file produced above is validated by doing a test conversion to
    DCAT format, and files that fail this validation are discarded.
 6. A combined DCAT file is assembled based on the validated spec files.
 7. That DCAT file is uploaded to S3 for publication on `data.jobtechdev.se`.
 8. All DCAT files generated in step 3 and then step 4 above is uploaded
    into its originating folder.

For DCAT conversion, this software is used: https://github.com/DIGGSweden/DCAT-AP-SE-Processor


# Configuration in this Git repo
 - `conf/catalog.json` - The catalog description, general for all APIs.
 - `conf/api-json-spec-urls.txt` - The list of API URLs.
 - `generate_dcat/Makefile` - A number of default settings, such as bucket address etc.


# Configuration in files in S3
 - In a bucket directory, store a file named `directory.dcat.json` to
   apply its containing information to all data files in that
   directory.  This program will generate a standalone
   `.dcat.xml`-file for each data file, containing complete metadata
   (combining `directory.dcat.json` and `conf/catalog.json`).
 - In a bucket directory, store a file named `collection.dcat.json` to
   create a group object, containing all the files in its directory. The
   information in `collection.dcat.json` will be combined with the information
   in `conf/catalog.json`.


# Usage

## Local container
First create a directory, for example `/tmp/tst/', containing two files:

config:
```
[default]
region = eu-central-1
output = json
```

credentials:
```
[default]
aws_access_key_id = ...
aws_secret_access_key = ...
```

Then run the system like this:
```
podman build -t djs3 .
podman run -v /tmp/tst:/secrets --rm -it djs3
```

## Openshift
This can be run as a cronjob in Openshift. See config files here:
<https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-prepare-static-web-infra>


## Locally
You can also run it locally, if you have all dependencies installed,
and use compatible versions of the tools (notably, BSD-tools will work
differently and probably fail).

```
make clean
make local
```

## Debugging the Makefile
The Makefile is getting a bit too big. Add `-x` to `.SHELLFLAGS` at the top to get a decent trace.


# Known limitations
- Does not handle RAML, YAML or Swagger/OpenAPI < 3.
