#!/bin/sh

export HOME=/tmp

ln -s /secrets $HOME/.aws

uv run --no-dev --frozen main.py
