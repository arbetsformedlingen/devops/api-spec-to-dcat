FROM alpine:3.21.3 AS build

WORKDIR /app

COPY --from=ghcr.io/astral-sh/uv:0.6 /uv /uvx /bin/
COPY main.py .
COPY entrypoint.sh .
COPY pyproject.toml .
COPY uv.lock .
COPY modules/ modules/
COPY conf/ conf/

ADD https://gitlab.com/arbetsformedlingen/devops/dcat-ap-se-processor-mirror/-/jobs/9193303574/artifacts/raw/target/dcat-ap-processor-0.0.2-SNAPSHOT.jar /app/app.jar

RUN chmod +x entrypoint.sh
RUN chmod a+xr app.jar

RUN apk add --no-cache python3 openjdk21-jre aws-cli

RUN uv sync --frozen --no-dev



###############################################################################
FROM build AS test

COPY tests/ tests/

RUN uv run --frozen main.py --help \
    && uv sync --frozen \
    && uv add pytest-cov \
    && uv run --frozen pytest --cov=modules tests \
    && touch /.tests-successful



###############################################################################
FROM build AS final

COPY --from=test /.tests-successful /

ENTRYPOINT ["/app/entrypoint.sh"]
