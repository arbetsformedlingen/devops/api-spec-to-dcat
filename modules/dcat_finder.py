import subprocess
import logging
from modules.blacklist import is_blacklisted
import os


def find_dcat_files(bucket):
    try:
        result = subprocess.run(
            ["aws", "s3", "ls", "--recursive", f"s3://{bucket}/"],
            stdout=subprocess.PIPE,
            check=True,
            text=True,
        )
        files = result.stdout.splitlines()
        files = [
            line[31:].strip()
            for line in files
            if line.strip() != ""
            and os.path.basename(line[31:].strip()) != "directory_link.dcat.json"
        ]
        valid_files = [
            line
            for line in files
            if line.endswith(".dcat.json") and not is_blacklisted(line)
        ]
        logging.info(f"Hittade {len(valid_files)} giltiga .dcat.json-filer.")
        return valid_files
    except subprocess.CalledProcessError as e:
        logging.error(f"Fel vid hämtning av S3-filer: {e}")
        return []
