import os
import subprocess
import logging
import shutil
import uuid

from modules import blacklist
from modules.file_operations import download_file
from . import dcatjson2dcatswagger
import tempfile

# Configure logging
logging.basicConfig(level=logging.INFO, format="%(levelname)s: %(message)s")


def validate_dcat(file_path: str, dcatjar: str, target_file: str, catalog_path: str) -> bool:
    """
    Validate a DCAT JSON file using the specified DCAT processor JAR file.

    This function runs the DCAT processor on the input file and checks the output for errors.
    If the validation is successful, the output is written to the target file.

    Args:
        file_path (str): The path to the input DCAT JSON file.
        dcatjar (str): The path to the DCAT processor JAR.
        target_file (str): The path to write the processed DCAT XML output.

    Returns:
        bool: True if validation and processing are successful, False otherwise.
    """
    try:
        if not os.path.exists(file_path):
            logging.error(f"Input file '{file_path}' does not exist.")
            return False

        tmpdir = tempfile.mkdtemp()

        # dcatprocessor requires a catalog.json file in the same directory as the input file
        shutil.copy(catalog_path, os.path.join(tmpdir, "catalog.json"))
        shutil.copy(file_path, tmpdir)

        # monkey-patching the str method to include stdout and stderr
        # this is useful for debugging when the subprocess fails, but keep in mind it will
        # have effect globally
        subprocess.CalledProcessError.__str__ = lambda self: (
            f"{self.cmd} returned exit status {self.returncode}\n"
            f"STDOUT: {self.stdout}\nSTDERR: {self.stderr}"
        )

        env = os.environ.copy()
        env["LANG"] = "sv_SE.UTF.8"
        result = subprocess.run(
            ["java", "-jar", dcatjar, "-d", tmpdir],
            capture_output=True,
            text=True,
            check=True,
            cwd=tmpdir,
            env=env,
        )
        if (
            "There are Errors" in result.stdout
            or "Kunde inte generera en dcat fil" in result.stdout
        ):
            logging.warning(
                f"Validering misslyckades för {file_path}. {result.stdout}\n{result.stderr}"
            )
            return False
        with open(target_file, "w", encoding="utf-8") as f:
            f.write(result.stdout)
        return True
    except subprocess.CalledProcessError as e:
        logging.error(f"Error validating {file_path}: {e}")
        return False
    finally:
        shutil.rmtree(tmpdir, ignore_errors=True)


def create_flat_dir(meta_dir: str, abs_flat_dir: str) -> str:
    """
    Create a flat directory by copying JSON files from the source directory into a target directory,
    renaming them uniquely to avoid conflicts.

    Args:
        meta_dir (str): The directory containing validated JSON files.
        abs_flat_dir (str): The target directory where flat JSON files will be stored.

    Returns:
        str: The absolute path of the flat directory.
    """
    logging.info("Creating flat directory...")
    os.makedirs(abs_flat_dir, exist_ok=True)

    for root, _, files in os.walk(meta_dir):
        # Skip the flat directory itself to prevent recursion
        if os.path.abspath(root) == os.path.abspath(abs_flat_dir):
            continue
        for file in files:
            if file.endswith(".json"):
                source_path = os.path.join(root, file)
                base, ext = os.path.splitext(file)
                new_name = f"{base}_{uuid.uuid4().hex}{ext}"
                dest_path = os.path.join(abs_flat_dir, new_name)
                shutil.copy2(source_path, dest_path)
                logging.info(f"Copied {source_path} to {dest_path}")
    return abs_flat_dir


def create_artifact(
    abs_flat_dir: str, target: str, catalog_path: str, dcatjar: str
) -> None:
    """
    Creates the final DCAT artifact by combining validated DCAT XML files with the catalog.

    This function copies the catalog.json file into the target directory and
    runs the DCAT processor JAR on the given flat directory, producing the final
    artifact. If errors are found in the generated output, a subprocess.CalledProcessError is raised.

    Args:
        abs_flat_dir (str): Absolute path of the directory containing validated .dcat.xml files.
        target (str): The file path where the final DCAT XML artifact will be saved.
        catalog_path (str): Path to the catalog.json file.
        dcatjar (str): Path to the DCAT processor JAR file.

    Raises:
        subprocess.CalledProcessError: If the DCAT processor outputs errors during artifact creation.
    """
    logging.info("Creating final DCAT artifact...")

    # Copy catalog.json to the target directory
    shutil.copy(catalog_path, os.path.join(abs_flat_dir, "catalog.json"))
    logging.info(f"Copied {catalog_path} to {abs_flat_dir}")

    # Run DCAT processor on the flat directory
    try:
        env = os.environ.copy()
        env["LANG"] = "sv_SE.UTF.8"
        result = subprocess.run(
            ["java", "-jar", dcatjar, "-d", abs_flat_dir],
            capture_output=True,
            text=True,
            check=True,
            cwd=abs_flat_dir,
            env=env,
        )

        # Save output to the target artifact file
        with open(target, "w", encoding="utf-8") as f:
            f.write(result.stdout)

        # Check for errors in the output from the DCAT processor
        if (
            "There are Errors" in result.stdout
            or "Kunde inte generera en dcat fil" in result.stdout
        ):
            logging.error("Errors found in generated DCAT artifact:")
            logging.error(result.stdout)
            raise subprocess.CalledProcessError(1, "java", output=result.stdout)

    except subprocess.CalledProcessError as e:
        logging.error(f"Failed to create DCAT artifact: {e}")
        if e.stdout:
            logging.error(f"Output: {e.stdout}")
        if e.stderr:
            logging.error(f"Error: {e.stderr}")
        raise e
    logging.info(f"DCAT artifact created at {target}")


def validate_and_move_dcat_files(staging: str, target: str, dcatjar: str, catalog_path: str) -> None:
    """
    Validate DCAT JSON files and move the successfully validated files from the staging
    directory to the target directory after converting them to DCAT XML format.

    Args:
        staging (str): The directory containing downloaded .dcat.json files.
        target (str): The directory where validated .dcat.xml files will be moved.
        dcatjar (str): The path to the DCAT processor JAR file used for validation.

    Returns:
        None
    """
    for root, _, files in os.walk(staging):
        for file in files:
            if file.endswith(".dcat.json"):
                file_path = os.path.join(root, file)
                target_file = file_path.replace(".dcat.json", ".dcat.xml")
                if validate_dcat(file_path, dcatjar, target_file, catalog_path):
                    relative_target = os.path.relpath(file_path, staging)
                    dest_path = os.path.join(target, relative_target)
                    os.makedirs(os.path.dirname(dest_path), exist_ok=True)
                    logging.info(
                        f"Validation complete. Moving {file_path} to {dest_path}"
                    )
                    shutil.move(file_path, dest_path)
                else:
                    logging.error(f"Validation failed for {file_path}")
                    os.remove(file_path)


# WARNING: This function has not been tested yet
def process_directory_dcat(bucket, base_url):
    staging_dir = "staging"
    logging.info("INFO] process-directory-dcat...")
    # List all files in the bucket recursively
    try:
        result = subprocess.run(
            ["aws", "s3", "ls", "--recursive", f"s3://{bucket}/"],
            capture_output=True,
            text=True,
            check=True,
        )
        lines = result.stdout.splitlines()
    except subprocess.CalledProcessError as e:
        logging.error(f"Error listing S3 bucket: {e}")
        return

    # Find any line containing directory.dcat.json
    dircat_lines = [
        line[31:].strip() for line in lines if "directory.dcat.json" in line
    ]
    if not dircat_lines:
        return

    # Process each found directory.dcat.json file
    for dirdcat in dircat_lines:
        # Remove the first 30 characters to extract the S3 key (mimicking perl -pe 's/^.{30} //')
        download_url = f"{base_url}{dirdcat}"
        logging.info(f"INFO] Downloading {download_url}...")
        metadata_file_path = os.path.join(staging_dir, dirdcat)
        dir_path = os.path.dirname(metadata_file_path)
        os.makedirs(dir_path, exist_ok=True)

        if not download_file(download_url, metadata_file_path):
            logging.warning(f"WARNING] Cannot download {download_url}")
            continue

        s3_path = os.path.dirname(dirdcat)
        try:
            result = subprocess.run(
                ["aws", "s3", "ls", f"s3://{bucket}/{s3_path}/"],
                capture_output=True,
                text=True,
                check=True,
            )
            s3_lines = result.stdout.splitlines()
        except subprocess.CalledProcessError as e:
            logging.error(f"Error listing S3 directory s3://{bucket}/{s3_path}/: {e}")
            os.remove(metadata_file_path)
            continue

        # For each line that does not contain a slash and then trim the first 30 characters
        datafiles = []
        for s3_line in s3_lines:
            if "/" in s3_line:
                continue
            # Mimic perl trimming of first 30 characters then stripping whitespace
            datafile_candidate = s3_line[30:].strip()
            if not blacklist.is_blacklisted(
                datafile_candidate
            ) and not datafile_candidate.endswith(".dcat.xml"):
                datafiles.append(datafile_candidate)

        # Process each datafile
        for datafile in datafiles:
            prep_url = f"{base_url}{s3_path}/{datafile}"
            logging.info(f"INFO] Preparing {prep_url}.dcat.json...")
            try:
                # Convert using dcatjson2dcatswagger, using the downloaded directory file as input.
                dcatjson2dcatswagger.convert_dcat_json_to_swagger(
                    input_file_path=metadata_file_path,
                    input_id=prep_url,
                    target=f"{dir_path}/{datafile}.dcat.json",
                    access_url=prep_url,
                )
            except Exception as e:
                logging.error(f"Error processing {prep_url}: {e}")
                continue

        try:
            os.remove(metadata_file_path)
        except OSError as e:
            logging.warning(f"Could not remove {metadata_file_path}: {e}")


def process_directory_link_dcat(bucket: str, base_url: str) -> None:
    """
    Process directory_link.dcat.json files from an S3 bucket.

    This function lists all the files in the specified S3 bucket,
    filters for files containing 'directory_link.dcat.json', downloads them,
    makes a backup copy with a '.org' extension, and converts the downloaded
    DCAT JSON file to Swagger format.

    Args:
        bucket (str): The name of the S3 bucket.
        base_url (str): The base URL to use for downloading the files.

    Returns:
        None
    """
    logging.info("Processing directory_link.dcat.json files...")

    try:
        # Check for directory_link.dcat.json files in bucket
        result = subprocess.run(
            ["aws", "s3", "ls", "--recursive", f"s3://{bucket}/"],
            capture_output=True,
            text=True,
            check=True,
        )

        # Filter for directory_link.dcat.json files by removing the first 31 characters from matching lines
        lines = result.stdout.splitlines()
        dir_dcats = [
            line[31:].strip() for line in lines if "directory_link.dcat.json" in line
        ]

        for dirdcat in dir_dcats:
            logging.info(f"Processing {dirdcat}...")

            # Download the file
            url = f"{base_url}{dirdcat}"
            file_path = os.path.join("staging", dirdcat)
            dir_path = os.path.dirname(file_path)
            os.makedirs(dir_path, exist_ok=True)
            if not download_file(url, file_path):
                logging.error(f"Error downloading {url}. Skipping.")
                continue

            shutil.copy(file_path, file_path + ".org")

            try:
                input_id = f"{base_url}{dir_path.replace('staging/', '')}/"
                dcatjson2dcatswagger.convert_dcat_json_to_swagger(
                    input_file_path=file_path,
                    input_id=input_id,
                    target=file_path,
                    access_url=input_id,
                    download_url="",
                )
            except Exception as e:
                logging.error(f"Error processing {dirdcat}: {e}")

    except subprocess.CalledProcessError as e:
        logging.error(f"Error listing S3 bucket: {e}")
