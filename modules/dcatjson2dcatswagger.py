import json
import os
import logging

logging.basicConfig(level=logging.INFO, format="%(levelname)s: %(message)s")


def convert_dcat_json_to_swagger(
    input_file_path,
    input_id,
    target,
    access_url="",
    download_url="",
    title_sv="",
    title_en="",
):
    """
    Convert DCAT JSON to DCAT Swagger format.

    Args:
        input_file_path (str): Path to input DCAT JSON file
        input_id (str): Input ID URL
        access_url (str, optional): Access URL
        download_url (str, optional): Download URL
        title_sv (str, optional): Swedish title
        title_en (str, optional): English title

    Returns:
        dict: The converted swagger template
    """
    with open("conf/swaggerwrapper.json", "r", encoding="utf-8") as f:
        swagger_template = json.load(f)

    with open("conf/catalog.json", "r", encoding="utf-8") as f:
        catalog = json.load(f)

    with open(input_file_path, "r", encoding="utf-8") as f:
        input_data = json.load(f)

    # Initialize required structures
    swagger_template["info"]["x-dcat"]["dcat-dataset"].setdefault("distribution", {})
    swagger_template["info"]["x-dcat"]["dcat-dataset"].setdefault("contactPoint", {})

    # Merge JSON files
    swagger_template["info"]["x-dcat"]["dcat-dataset"].update(input_data)
    swagger_template["info"]["x-dcat"].setdefault("dcat-catalog", {})
    swagger_template["info"]["x-dcat"]["dcat-catalog"].update(
        catalog.get("dcat-catalog", {})
    )

    # Check if input has a 'distribution' field
    has_distribution = "distribution" in input_data
    if not has_distribution:
        # If no distribution field is present (likely using distributionN fields),
        # remove the default distribution
        if "distribution" in swagger_template["info"]["x-dcat"]["dcat-dataset"]:
            del swagger_template["info"]["x-dcat"]["dcat-dataset"]["distribution"]
    else:
        # Update URLs and distribution about only if we're keeping the distribution
        if not swagger_template["info"]["x-dcat"]["dcat-dataset"]["distribution"].get("accessURL"):
            swagger_template["info"]["x-dcat"]["dcat-dataset"]["distribution"]["accessURL"] = (
            access_url or input_id
            )

        if download_url and not swagger_template["info"]["x-dcat"]["dcat-dataset"]["distribution"].get("downloadURL"):
            swagger_template["info"]["x-dcat"]["dcat-dataset"]["distribution"][
                "downloadURL"
            ] = download_url

        swagger_template["info"]["x-dcat"]["dcat-dataset"]["distribution"]["about"] = (
            f"{input_id}#distrib"
        )

    # Update contact and about
    swagger_template["info"]["x-dcat"]["dcat-dataset"]["contactPoint"]["about"] = (
        f"{input_id}#contact"
    )
    swagger_template["info"]["x-dcat"]["dcat-dataset"]["about"] = f"{input_id}#about0"

    # Handle titles
    title_sv = title_sv or os.getenv("title_sv", "")
    title_en = title_en or os.getenv("title_en", "")

    if title_sv:
        swagger_template["info"]["x-dcat"]["dcat-dataset"].setdefault("title-sv", "")
        swagger_template["info"]["x-dcat"]["dcat-dataset"]["title-sv"] += (
            f" - {title_sv}"
        )
        if has_distribution:
            swagger_template["info"]["x-dcat"]["dcat-dataset"]["distribution"][
                "title-sv"
            ] = swagger_template["info"]["x-dcat"]["dcat-dataset"]["title-sv"]

    if title_en:
        swagger_template["info"]["x-dcat"]["dcat-dataset"].setdefault("title-en", "")
        swagger_template["info"]["x-dcat"]["dcat-dataset"]["title-en"] += (
            f" - {title_en}"
        )
        if has_distribution:
            swagger_template["info"]["x-dcat"]["dcat-dataset"]["distribution"][
                "title-en"
            ] = swagger_template["info"]["x-dcat"]["dcat-dataset"]["title-en"]

    logging.info(f"Convert {input_file_path} to {target}")
    with open(target, "w", encoding="utf-8") as f:
        json.dump(swagger_template, f, indent=2, ensure_ascii=False)

    return swagger_template
