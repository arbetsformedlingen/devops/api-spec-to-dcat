import os
import logging
import sys
import requests
import shutil
import subprocess

from modules import dcat_finder
from . import dcatjson2dcatswagger

# Configure logging
logging.basicConfig(level=logging.INFO, format="%(levelname)s: %(message)s")


def verify_env(required_tools: list[str]) -> None:
    """
    Verify that all required tools are available in the system's PATH.

    Args:
        required_tools (list[str]): A list of tool names to check.

    Raises:
        SystemExit: If any of the required tools is not installed.
    """
    for tool in required_tools:
        if not shutil.which(tool):
            logging.error(f"Please install {tool}")
            sys.exit(1)
    logging.info("Environment verification completed.")


def create_directories(dirs: list[str]) -> None:
    """
    Create directories from the provided list.

    Args:
        dirs (list[str]): A list of directory paths to create.

    Returns:
        None
    """
    for directory in dirs:
        os.makedirs(directory, exist_ok=True)
        logging.info(f"Directory '{directory}' created or already exists.")


def download_file(url: str, destination: str) -> bool:
    """
    Download a file from the specified URL and save it to the destination.

    Args:
        url (str): The URL of the file to download.
        destination (str): The file path where the downloaded file should be saved.

    Returns:
        bool: True if the download was successful, False otherwise.
    """
    try:
        os.makedirs(os.path.dirname(destination), exist_ok=True)
        response = requests.get(url)
        response.raise_for_status()
        with open(destination, "wb") as f:
            f.write(response.content)
        logging.info(f"File downloaded from {url} to {destination}.")
    except Exception as e:
        logging.warning(f"Cannot download from {url}: {e}. Skipping.")
        return False

    if not os.path.exists(destination) or os.path.getsize(destination) == 0:
        logging.error(f"Downloaded file {destination} is missing or empty.")
        return False

    return True


def download_file_list(url: str) -> list[str]:
    """
    Downloads and parses a file containing a list of URLs.

    This function fetches the content from the given URL or local file path, processes it to
    filter out empty lines and lines that do not start with 'http', and returns a list of valid URLs.

    Args:
        url (str): The URL or local file path of the text file containing the URL list.

    Returns:
        list[str]: A list of URLs.
    """
    try:
        if url.startswith("http"):
            response = requests.get(url)
            response.raise_for_status()
            content = response.text
        else:
            with open(url, "r") as f:
                content = f.read()

        return [
            line.strip()
            for line in content.splitlines()
            if line.strip() and line.strip().startswith("http")
        ]
    except Exception as e:
        logging.error(f"Error reading URL list from {url}: {e}")
        return []


def upload_artifact(bucket: str, artifact: str, target: str) -> None:
    """
    Uploads the DCAT artifact to the specified S3 bucket.

    Args:
        bucket (str): The S3 bucket name.
        artifact (str): The local path to the DCAT artifact file.
        target (str): The target path within the S3 bucket where the artifact will be stored.

    Raises:
        subprocess.CalledProcessError: If the AWS CLI command fails.
    """
    try:
        _ = subprocess.run(
            ["aws", "s3", "cp", artifact, f"s3://{bucket}/{target}"],
            capture_output=True,
            text=True,
            check=True,
        )
        logging.info("DCAT artifact uploaded to S3")
    except subprocess.CalledProcessError as e:
        logging.error(f"Could not upload DCAT artifact to S3: {e}")
        if e.stdout:
            logging.error(f"Output: {e.stdout}")
        if e.stderr:
            logging.error(f"Error: {e.stderr}")
        raise e


def download_and_convert_metadata(BUCKET: str, staging: str, BASEURL: str) -> None:
    """
    Downloads DCAT metadata files from an S3 bucket and converts them to Swagger format.

    This function uses dcat_finder to identify DCAT JSON files in the specified S3 bucket,
    downloads each file to the staging directory, creates a backup copy with a ".org" extension,
    and converts the downloaded DCAT JSON file to Swagger format using dcatjson2dcatswagger.

    Args:
        BUCKET (str): The S3 bucket name.
        staging (str): The local staging directory for downloaded files.
        BASEURL (str): The base URL used to construct the file download URL.

    Returns:
        None
    """
    s3_files = dcat_finder.find_dcat_files(BUCKET)
    for s3_file in s3_files:
        destination = os.path.join(staging, s3_file)
        logging.info(
            f"Downloading metadata file from s3 {s3_file} to {destination} - this file will be converted to swagger format."
        )
        download_file(f"{BASEURL}{s3_file}", destination)
        shutil.copy(destination, destination + ".org")
        datafile = destination.removesuffix(".dcat.json")
        datafile = datafile.replace(f"{staging}/", "")
        dcatjson2dcatswagger.convert_dcat_json_to_swagger(
            input_file_path=destination,
            input_id=f"{BASEURL}{datafile}",
            target=destination,
            # download_url=url, # access_url='', title_sv='', title_en=''
        )
