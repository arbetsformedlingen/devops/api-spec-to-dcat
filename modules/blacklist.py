import re

BLACKLIST_PATTERNS = re.compile(
    r"^(index\.html|public-tokens|dcatotron|Nginx-Fancyindex-Theme-light|swagger|rss|50x\.html|README|directory\.dcat\.json|collection\.dcat\.json)$",
    re.IGNORECASE,
)


def is_blacklisted(s):
    """Check if a filename matches any blacklisted pattern"""
    filename = s.split("/")[-1] if "/" in s else s
    return bool(BLACKLIST_PATTERNS.search(filename))
